import angular from 'angular';
import uirouter from 'angular-ui-router';
(function() {
  var app = angular.module('app', [uirouter]);
  app.run(function($rootScope, $location, $state, MyLoginService) {
    $rootScope.$on('$stateChangeStart', 
      function(event, toState, toParams, fromState, fromParams){ 
        
      });
		// not authenticate yet
      if(!MyLoginService.isAuthenticated()) {
        $state.transitionTo('login');
      }
  });
  
  app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
      .state('login', {
        url : '/login',
        templateUrl : 'login.html',
        controller : 'MyLoginController'
      })
      .state('register', {
        url : '/register',
        templateUrl : 'register.html',
        controller : 'MyRegisterController'
      })
      .state('home', {
        url : '/home',
        templateUrl : 'home.html',
        controller : 'MyHomeController',
		auth:true
      });
  }]);

  app.controller('MyLoginController', function($scope, $rootScope, $stateParams, $state, MyLoginService) {
    $rootScope.title = "Login";


    $scope.submitMyForm = function() {
	if($scope.username.length >= 5 && $scope.password.length >= 6){
      if(MyLoginService.login($scope.username, $scope.password)) {
		$rootScope.username=$scope.username;
        $scope.error = '';
        $scope.username = '';
        $scope.password = '';
        $state.transitionTo('home');
      } else {
        $scope.error = "Wrong Username or PWD";
      }   
	  
	} else {
	
		$scope.error = "Username should be bigger than 5 char and password should be bigger than 6 char";
		
	}
    };
  });

  app.controller('MyRegisterController', function($scope, $rootScope, $stateParams, $state, MyRegisterService) {
	
    $rootScope.title = "Register";

    $scope.submitMyFormForRegister = function() {
		
	if($scope.username.length >= 5 && $scope.password.length >= 6){
		if($scope.password === $scope.passwordRepeat){
			
				 if(MyRegisterService.register($scope.username, $scope.password)) {
					$rootScope.username=$scope.username;
					$scope.error = '';
					$scope.username = '';
					$scope.password = '';
					$state.transitionTo('login');
				  } else {
					$scope.error = "Wrong Username or PWD";
				  }  
			
		} else {
			
			$scope.error = "you should write same pass";
		}
		
	} else {
			
		$scope.error = "Username should be bigger than 5 char and password should be bigger than 6 char";
	}
   
    };
  });
  
  app.controller('MyHomeController', function($scope, $rootScope, $stateParams, $state, MyLoginService) {
	  if(!MyLoginService.isAuthenticated()) {
		$state.transitionTo('login');
	  }
	$rootScope.title = "";
    $rootScope.homeTitle = "Welcome Home";
    console.log("HomeController");
  });
  
  app.factory('MyLoginService', function() {
    var admin = 'john@doe.com';
    var pass = 'johndoe';
    var isAuthenticated = false;
    return {
      login : function(username, password) {
        isAuthenticated = username === admin && password === pass;
        return isAuthenticated;
      },
      isAuthenticated : function() {
        return isAuthenticated;
      }
    };
    
  });
  app.factory('MyRegisterService', function() {

    return {
		register : function(username, password){
			
			return true;
		}
    };
    
  });
  
  })();